package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


/**
 * Created by sc16sr on 02/11/17.
 */
public class DateTest
{
  private Date lowestBoundry;
  private Date highestBoundry;
  private Date inbetweenBoundry;
  private Date leapHighestBound;
  private Date februaryBoundry;
  private Date leapLowestBound;
  private Date leapInbetweenBound;


  @Before
  public void dates()
  {
    lowestBoundry = new Date(0,1,1);
    highestBoundry = new Date(2017, 12, 31);
    inbetweenBoundry = new Date(2017, 6, 15);
    leapHighestBound = new Date(2000,12,31);
    februaryBoundry = new Date(2001, 2, 28);
    leapLowestBound = new Date(2000, 2, 29);
    leapInbetweenBound = new Date(2000, 6, 15);
  }

  @Test
  public void validDates()
  {
    assertThat(lowestBoundry.toString(), is("0000-01-01"));
    assertThat(highestBoundry.toString(), is("2017-12-31"));
    assertThat(inbetweenBoundry.toString(), is("2017-06-15"));
    assertThat(leapHighestBound.toString(), is("2000-12-31"));
    assertThat(februaryBoundry.toString(), is("2001-02-28"));
    assertThat(leapLowestBound.toString(), is("2000-02-29"));
    assertThat(leapInbetweenBound.toString(), is("2000-06-15"));
  }



  @Test(expected = IllegalArgumentException.class)
  public void yearToLow()
  {
    new Date(-1, 1, 31);
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthToHigh()
  {
    new Date(1997, 13, 18);
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthToLow()
  {
    new Date(1997, 0, 18);
  }

  @Test(expected = IllegalArgumentException.class)
  public void thirtyOneDayMonthToHigh()
  {
    new Date(1997, 1, 32);
  }

  @Test(expected = IllegalArgumentException.class)
  public void thirtyDayMonthToHigh()
  {
    new Date(1997, 4, 31);
  }

  @Test(expected = IllegalArgumentException.class)
  public void FebruaryToHigh()
  {
    new Date(1997, 2, 29);
  }

  @Test(expected = IllegalArgumentException.class)
  public void leapFebruaryToHigh()
  {
    new Date(2000, 2, 30);
  }

  @Test(expected = IllegalArgumentException.class)
  public void thirtyOneDayMonthToLow()
  {
    new Date(1997, 1, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void thirtyDayMonthToLow()
  {
    new Date(1997, 4, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void FebruaryToLow()
  {
    new Date(1997, 2, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void leapFebruaryToLow()
  {
    new Date(2000, 2, 0);
  }


  @Test
  public void equals()
  {
;
    assertThat(lowestBoundry.equals(new Date(0,1,1)), is(true));
    assertThat(new Date(0,1,1).equals(lowestBoundry), is(true));
    assertThat(lowestBoundry.equals(lowestBoundry), is(true));
    assertThat(lowestBoundry.equals(highestBoundry), is(false));
    assertThat(highestBoundry.equals(lowestBoundry), is(false));
    assertThat(lowestBoundry.equals(new Date(2,10,13)), is(false));
  }


  @Test
  public void dayOfYear()
  {
    Date leapYearHighestBound = new Date(2000,12,31);
    assertThat(lowestBoundry.getDayOfYear(), is(1));
    assertThat(highestBoundry.getDayOfYear(), is(365));
    assertThat(leapYearHighestBound.getDayOfYear(), is(366));
    assertThat(februaryBoundry.getDayOfYear(), is(59));

  }


}