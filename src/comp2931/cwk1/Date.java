// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.List;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Simple representation of a date...
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * creates a date based off the systems time.
   * Default constructor
   */
  public Date()
  {

    Calendar date = Calendar.getInstance();
    int y = date.get(Calendar.YEAR);
    int m = date.get(Calendar.MONTH);
    int d = date.get(Calendar.DAY_OF_MONTH);

    year = y;
    month = m+1;
    day = d;

  }

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d)
  {
    setYear(y);
    setMonth(m);
    setDay(y, m, d);

  }

  private void setYear(int y)
  {
    //year checker
    if(y < 0)
    {
      throw new IllegalArgumentException("invalid year has to be greater than 0");
    }
    year = y;
  }

  private void setMonth(int m)
  {
    //month checker
    if(m > 12 || m < 1)
    {
      throw new IllegalArgumentException("invalid month, please make sure it is between 1-12");
    }
    month = m;
  }

  private void setDay(int y, int m, int d)
  {
    List<Integer> thirtyDayMonths = Arrays.asList(4, 6, 9, 11);
    List<Integer> thirtyOneDayMonths = Arrays.asList(1, 3, 5, 7, 8, 10, 12);
    int february = 2;

    //day checker
    if(d < 1)
    {
      throw new IllegalArgumentException("invalid day, greater than 0");
    }

    else if(thirtyOneDayMonths.contains(m) && d > 31)
    {
      throw new IllegalArgumentException("invalid day, please make sure it is between 1-31");
    }

    //leap year
    else if(m == 2 && ((y % 4) == 0) && d > 29)
    {
      throw new IllegalArgumentException("invalid day, please make sure it is between 1-28");
    }
    //non leap year
    else if(m == february && ((y % 4) != 0) && d > 28)
    {
      throw new IllegalArgumentException("invalid day, please make sure it is between 1-28");
    }
    else if(thirtyDayMonths.contains(m) && d > 30)
    {
      throw new IllegalArgumentException("invalid day, please make sure it is between 1-28");
    }

    day = d;
  }


  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }

  /**
   *
   *Compares the Date inputted to see if it is equal.
   *
   * <p>An example of the required format when called:</p>
   * <pre>
   *    d1.equals(d2) - d1 = a date , d2 = a second date.
   * </pre>
   *
   *@return A true / false value depending on whether they are equal or not
   *@param object A pre-made date to test
   */
  @Override
  public boolean equals(Object object)
  {
    if (this == object)
    {
      return true;
    }
    if (!(object instanceof Date))
    {
      return false;
    }

    Date otherRecord = (Date) object;

    return year == otherRecord.year &&
        month == otherRecord.month &&
        day == otherRecord.day;

  }

  /**
   *
   *Gets and returns a number to represent the day number in the year
   * i.e. 1 January = 1 and 31 December on a non leap year is 365
   *
   * <p>An example of the required format when called:</p>
   * <pre>
   *    d1.equals() - d1 = a date
   * </pre>
   *
   *@return A integer representing the day of the year that has been given
   */
  public int getDayOfYear()
  {
    int dayOfYear;

    Calendar date = Calendar.getInstance();
    date.set(Calendar.YEAR, this.getYear());
    date.set(Calendar.MONTH, this.getMonth()-1);
    date.set(Calendar.DAY_OF_MONTH, this.getDay());

    dayOfYear = date.get(Calendar.DAY_OF_YEAR);

    return dayOfYear;
  }
}
